import flask
from flask import render_template, jsonify, request, redirect, url_for
import requests as r
import json
from datetime import datetime
#import pandas as pd
#from jinja2 import Template

pd.set_option('display.max_colwidth', -1)

filename = 'data.json'

app = flask.Flask(__name__)

def process_form(args, filename = filename):
    dat = json.load(open(filename))
    dat.append({k: v for k, v in args.items()})
    json.dump(dat, open(filename, 'w'))
    return True

def process_new_workout(args, filename = 'workouts.json'):
    workouts = json.load(open(filename))
    new_workouts = list(args.values())
    repeat = [w for w in new_workouts if w in workouts]
    if repeat:
        return False, repeat
    workouts += new_workouts
    json.dump(workouts, open(filename, 'w'))
    return True, []

@app.route('/')
def index():
    now = datetime.now().strftime('%Y-%m-%dT%H:%M')
    table = pd.read_json(filename).sort_values('date')
    table['foo'] = '<a href="url_for">Source of Truth</a>'
    dat = json.load(open(filename))
    if dat:
        workout = dat[-1]
        all_workouts = json.load(open("workouts.json"))
    else:
        workout = {}
    form = [{'value': workout.get('workout'), 'name': 'workout', 'type': 'text'}]
    form.append({'value': workout.get('weight'), 'name': 'weight', 'type': 'number'})
    form.append({'value': workout.get('reps'), 'name': 'reps', 'type': 'number'})
    form.append({'value': workout.get('sets'), 'name': 'sets', 'type': 'number'})
    form.append({'value': now, 'name': 'date', 'type': 'datetime-local'})
    if request.args:
        process_form(request.args)
        return flask.redirect(url_for('index'))
    #now = datetime.now().strftime('%m/%d/%Y, %H:%M %S')

    return render_template('index.html', form_data = form, table = table, all_workouts = all_workouts)

#
@app.route('/register_workout')
def register_workout():
    if request.args:
        status, repeats = process_new_workout(request.args)
        if not status:
            return render_template('register.html', repeats = repeats)
        return redirect(url_for('index'))
    return render_template('register.html')

@app.route('/get_stats')
def get_stats():
    now = datetime.now().strftime('%Y-%m-%dT%H:%M')
    workout = request.args.get('workout', 'Military Press')
    table = pd.read_json(filename).sort_values('date')
    data = table[table['workout'] == workout].iloc[-1]
    data['date'] = now# data['date'].strftime('%Y-%m-%dT%H:%M')
    return data.to_json()

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 5679, debug = True, threaded = True)
